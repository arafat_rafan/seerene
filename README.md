# Seerene

# Installation

Clone repository
```bash
git clone https://arafat_rafan@bitbucket.org/arafat_rafan/seerene.git
```
## Prerequisite

Install django-extra-views

```bash
pip3 install django-extra-views
```
## Create a super user

```bash
python3 manage.py createsuperuser
```

## Running application

Run following commands sequentially in the rood directory of the project where manage.py available to access the application from the browser

```bash
python3 manage.py makemigrations restaurant
```

```bash
python3 manage.py migrate
```

```bash
python3 manage.py runserver
```

Go to [Restaurant App](http://127.0.0.1:8000/)

## Running the test

```bash
python3 manage.py test restaurant
```