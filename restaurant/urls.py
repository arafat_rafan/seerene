from django.urls import path

from . import views

app_name = 'restaurant'
urlpatterns = [
    path('', views.GeneralView.as_view(), name='restaurant_general_view'),
    path('create/', views.RestaurantCreate.as_view(), name='restaurant_create'),
    path('menu/create/', views.RestaurantMenuCreate.as_view(), name='restaurant_menu_create'),
    path('<int:pk>/', views.RestaurantUpdate.as_view(), name='restaurant_update'),
]
