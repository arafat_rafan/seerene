from django.test import TestCase
from django.urls import reverse

from .models import Restaurant, RestaurantMenuType


class RestaurantListingViewTests(TestCase):

    def test_redirect_restaurant_create_view_if_not_logged_in(self):
        response = self.client.get(reverse('restaurant:restaurant_create'))
        self.assertRedirects(response, '/accounts/login/?next=/restaurant/create/')

    def test_redirect_restaurant_update_view_if_not_logged_in(self):
        response = self.client.get(reverse('restaurant:restaurant_update', kwargs={'pk': 1}))
        self.assertRedirects(response, '/accounts/login/?next=/restaurant/1/')

    def test_redirect_restaurant_menu_view_if_not_logged_in(self):
        response = self.client.get(reverse('restaurant:restaurant_menu_create'))
        self.assertRedirects(response, '/accounts/login/?next=/restaurant/menu/create/')

    def test_no_restaurant_found(self):
        response = self.client.get(reverse('restaurant:restaurant_general_view'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "There is no restaurant!")
        self.assertQuerysetEqual(response.context['restaurant_list'], [])


class RestaurantAPITests(TestCase):

    def setUp(self):
        restaurantMenu1 = RestaurantMenuType.objects.create(name='{"menu1": "menu1"}')
        restaurantMenu2 = RestaurantMenuType.objects.create(name='{"menu2": "menu2"}')
        restaurant1 = Restaurant.objects.create(name='restaurant1', menu=restaurantMenu1, )
        restaurant2 = Restaurant.objects.create(name='restaurant2', menu=restaurantMenu2, )

    def test_all_restaurant_found(self):
        initial_queryset_count = Restaurant.objects.all().count()
        response = self.client.get(reverse('restaurant:restaurant_general_view'))
        self.assertEqual(response.status_code, 200)
        response_queryset_count = response.context['restaurant_list'].count()
        self.assertEqual(response_queryset_count, initial_queryset_count)

    def test_query_filter(self):
        url = '{url}?{filter}={value}'.format(
            url=reverse('restaurant:restaurant_general_view'),
            filter='q', value='restaurant1')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response_queryset_count = response.context['restaurant_list'].count()
        self.assertEqual(response_queryset_count, 1)
