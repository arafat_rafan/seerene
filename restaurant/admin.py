from django.contrib import admin

from .models import Restaurant, RestaurantMenuType

admin.site.register(Restaurant)
admin.site.register(RestaurantMenuType)
